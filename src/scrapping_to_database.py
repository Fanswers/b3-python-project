from src.infrastructure.scrapping.CSV_creation import dataframe_to_csv
from src.infrastructure.mariadb.database import database_insert_dataframe


if __name__ == '__main__':
    # Scrapp graphics card and save theme as a csv in Datasets folder
    dataframe_to_csv("graphics_card_ldlc.csv")
    # Insert data from the csv created previously into the database
    database_insert_dataframe("graphics_card_ldlc.csv")