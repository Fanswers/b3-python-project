# Graphic cards Scrapping

## Table of content

- [Description](#description)
- [Interface](#interface)
  * [Display Data](#display-data)
  * [Manipulate Data](#manipulate-data)
  * [Launch](#launch)
- [API](#api)
  * [Route](#route)
  * [Launch](#launch-1)
- [Installation](#installation)
  * [Install Requirements](#install-requirements)
    + [Create virtual environment](#create-virtual-environment)
    + [Install Packages](#install-packages)
  * [Database](#database)
    + [graphic_cards table](#graphic-cards-table)
    + [Link database](#link-database)
  * [Scrapp data](#scrapp-data)
    + [How it works](#how-it-works)
    + [Launch script](#launch-script)
- [Improvements](#improvements)



## Description
This project aim to scrapp data of graphic cards on computer components sites, store theme in a database, create an api to access to these data and display or manipulate theme on an interface.


## Interface
The interface has been made with Streamlite.<br>
[Streamlit Documentation](https://docs.streamlit.io/)


### Display Data
In the display section, you can see all graphics card and apply filters on theme.<br>
![Alt text](/Img/full_display.png?raw=true "Optional Title")

### Manipulate Data
In the manipulate section, you can add, modify and delete a graphics card.<br>
![Alt text](/Img/full_manipulate.png?raw=true "Optional Title")

### Launch
You can only run the API after you've down all the installation steps.<br>
To launch the interface, run this command in your virtual environment :<br>
``
streamlit run b3-python-project\streamlit_app\interface.py
``

## API
The API has been made with Flask restful.<br>
[Flask Restful Documentation](https://flask-restful.readthedocs.io/en/latest/)

### Route

| Description     | Route                                                         | Method                              |
|-----------------|---------------------------------------------------------------|-------------------------------------|
| all cards       | ``/cards``                                                    | ``get``                             |
| card by id      | ``/cards/id``                                               | ``get``                             |
| card by price   | ``/cards/pricemin-pricemax``                              | ``get``                             |
| all filter      | ``/cards/pricemin-pricemax/name/marketplace/dispo`` | ``get`` ``put`` ``post`` ``delete`` |                                                               |         |
| cards max price | ``/max_price``                                                | ``get``                             |

### Launch
You can only launch the API after you've down all the installation steps.<br>
To launch it, run the script ``main.py`` located in the ``src/`` folder at the root of the project.


## Installation

### Install Requirements
After you've cloned the project, you have to install the packages needed for the project.<br />
I advise you do it in a virtual environment.<br>
[Python Virtual Environment Documentation](https://docs.python.org/3/tutorial/venv.html#)

#### Create virtual environment
Go to the root of this project and do this command :

````
...\b3-ml-project> py -m venv .venv
````

Now you can see that a .venv file has been created, this is the virtual environment.<br>
To go inside :

````
...\b3-ml-project> .\.venv\Scripts\activate
````

Now your path look like this :
````
(.venv) ...\b3-ml-project>
````

If you need to quit the virtual environment :

````
(.venv) ...\b3-ml-project> deactivate
````

#### Install Packages
It only remains you to install all packages needed
````
(.venv) ...\b3-ml-project> py -m pip install -r requirements.txt
````


### Database
We need to store the data that we will scrapp in a database.<br />
So you gonna create a MySQL based database with one table name ``graphics_cards``.<br>
I've choose to used a MariaDB database.<br>
[MariaDB Server Documentation](https://mariadb.com/kb/en/documentation/)

#### graphic_cards table
````mysql
graphic_cards(
    card_id int auto_increment,
    name varchar(255),
    description varchar(255),
    price int,
    marketplace varchar(255),
    dispo varchar(255),
    primary key(card_id)
);
````

#### Link database
The api need to communicate with the database to access to the data.<br>
So to link the database to the project, you have to fill the `.env`  file as the root of the project.

````text
DB_HOST=127.0.0.1
DB_NAME=graphic_cards
DB_USERNAME=root
DB_PASSWORD=0000
````

### Scrapp data

#### How it works
The scrapping script will launch one function that will scrapp data thanks to selenium, and then save theme as a csv file in the Datasets folder.<br>
An other function will be launch to insert the csv that we previously created in the database.<br>
[Selenium Python Documentation](https://selenium-python.readthedocs.io/)

#### Launch script
There is one more step before launching the script that will scrapp data and store theme on the database.<br>
You need to locate you're web driver file and indicate this path at the line ``8`` of this file :<br>
``b3-python-project\src\infrastructure\scrapping\data_scrapping.py``
````python
browser = webdriver.Chrome(r'C:\chromedriver.exe')
````
Now that everything is good we can launch the script.<br>
The script is located at the root of the project and he's named ``scrapping_to_database.py``<br>
Now you can check your database and you will see all the graphic_cards.


## Improvements

- More site
- Notify when card is in stock
- Clean/Opti code
- Compare 2 or more cards
