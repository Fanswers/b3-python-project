from src.infrastructure.scrapping.data_scrapping import ldlc_multiple_page_scrapping
from pathlib import Path
import pandas as pd


# Dataframe to CSV
def dataframe_to_csv(name):
    data = ldlc_multiple_page_scrapping(8)

    filepath = Path(r"../Datasets/" + name)
    data.to_csv(filepath, index=False)


# CSV to dataframe
def csv_to_dataframe(name):
    filepath = Path(r"../Datasets/" + name)
    df_graphic_cards = pd.read_csv(filepath)

    return df_graphic_cards
