from src.infrastructure.api.api import app

# Launch api
if __name__ == '__main__':
    app.run(debug=True)
