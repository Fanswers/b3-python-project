import mysql.connector
from mysql.connector import errorcode
import os
from dotenv import load_dotenv
from src.infrastructure.scrapping.CSV_creation import csv_to_dataframe

# Load .env file
load_dotenv()

# Database connection config
config = {
  'user': os.getenv('DB_USERNAME'),
  'password': os.getenv('DB_PASSWORD'),
  'host': os.getenv('DB_HOST'),
  'database': os.getenv('DB_NAME'),
  'raise_on_warnings': True
}


# Returns graphic_cards table to dictionary
def full_table_to_dict(query):
  table = database_get(query)
  attribut = ('name', 'description', 'price', 'marketplace', 'dispo')
  CARDS = {}
  CARD = {}

  i = 1
  for cards in table:
    for j in range(5):
      CARD[attribut[j]] = cards[j + 1]
    CARDS['card' + str(i)] = CARD
    CARD = {}
    i += 1

  return CARDS


# Insert dataframe in graphics_card table with name of dataframe as parameter
def database_insert_dataframe(name):
  try:
    cnx = mysql.connector.connect(**config)

    cursor = cnx.cursor()

    df = csv_to_dataframe(name)

    for i, row in df.iterrows():
      sql = """INSERT INTO graphic_cards (name, description, price, marketplace, dispo) 
                             VALUES (%s, %s, %s, %s, %s) """
      cursor.execute(sql, tuple(row))
      cnx.commit()

    cursor.close()

    print(cursor.rowcount, "Record inserted successfully")

  except mysql.connector.Error as error:
    print("Failed to insert record into MySQL table {}".format(error))

  finally:
    if cnx.is_connected():
      cursor.close()
      cnx.close()
      print("MySQL connection is closed")


# Execute select query in database
def database_get(query):
  try:
    cnx = mysql.connector.connect(**config)
  except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
      print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
      print("Database does not exist")
    else:
      print(err)

  cursor = cnx.cursor()

  cursor.execute(query)

  data = cursor.fetchall()

  cursor.close()
  cnx.close()

  return data


# Execute query in database
def database_insert(query):
  try:
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    cursor.execute(query)
    cnx.commit()
    cursor.close()
    print(cursor.rowcount, "Record inserted successfully")

  except mysql.connector.Error as error:
    print("Failed to insert record into MySQL table {}".format(error))

  finally:
    if cnx.is_connected():
      cursor.close()
      cnx.close()
      print("MySQL connection is closed")