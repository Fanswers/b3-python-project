import requests
import streamlit as st
from requests import get
import pandas as pd


### Sidebar ###

# Choose between manipulate and display data
data_choice = st.sidebar.selectbox(
    "Display or Manipulate graphic cards data ?",
    ("Display", "Manipulate")
)


# Define input place_holder
card_json = {"card_id": '',
             "name": '',
             "description": '',
             "price": '',
             "marketplace": '',
             "dispo": '',
             }


# id input
if data_choice == 'Manipulate':
    id = st.sidebar.text_input('Card id')

    if id:
        card_json = get(f"http://localhost:5000/cards/{id}").json()


# name input
name = st.sidebar.text_input('Card name', card_json['name'])


# description input
if data_choice == 'Manipulate':
    description = st.sidebar.text_input('Card description', card_json['description'])


# price input
if data_choice == 'Display':
    price = st.sidebar.slider(
        "Price:",
        value=(0, int(get('http://localhost:5000/max_price').json()['max_price'] + 1)))

if data_choice == 'Manipulate':
    price = st.sidebar.text_input('Card price', card_json['price'])


# marketplace input
marketplace = st.sidebar.selectbox(
        'Marketplace',
        ('', 'ldlc', 'materiel.net'))


# disponibility input
dispo = st.sidebar.selectbox(
        'Disponibility',
        ('', 'EN STOCK', 'RUPTURE', 'ENTRE 7/15 JOURS', 'SOUS 7 JOURS', '+ DE 15 JOURS'))


# apply placeholder
if data_choice == 'Manipulate':
    card_json['card_id'] = id
    card_json['name'] = name
    card_json['description'] = description
    card_json['price'] = price
    card_json['marketplace'] = marketplace
    card_json['dispo'] = dispo


### Content ###

# Display title
if data_choice == 'Display':
    display_title = '<h1 style="font-family:Lucida Console; font-size:35px;">Display Graphic Cards data</h1>'
    st.markdown(display_title, unsafe_allow_html=True)

# Manipulate title
if data_choice == 'Manipulate':
    manipulate_title = '<h1 style="font-family:Lucida Console; font-size:35px;">Manipulate Graphic Cards data</h1>'
    st.markdown(manipulate_title, unsafe_allow_html=True)


# display data as dataframe
if data_choice == 'Display':
    df = pd.DataFrame.from_dict(
        get(f"http://localhost:5000/cards/{price[0]}-{price[1]}/%{name}%/%{marketplace}%/%{dispo}%").json(),
        orient='index')
    selected_title = f'<h2 style="font-family:Lucida Console; font-size:25px;">{df.shape[0]} graphic cards selected</h2>'
    st.markdown(selected_title, unsafe_allow_html=True)
    st.dataframe(df)

if data_choice == 'Manipulate':
    df = pd.DataFrame(data={'name': name, 'description': description, 'price': price, 'marketplace': marketplace, 'disponibility': dispo}, index=['card'+str(id)])
    st.dataframe(df)


# data manipulation button
if data_choice == 'Manipulate':
    col1, col2, col3 = st.columns(3)

    with col1:
        if st.button('Add card'):
            requests.post('http://localhost:5000/cards', json=card_json).json()

    with col2:
        if st.button('Apply'):
            requests.put('http://localhost:5000/cards', json=card_json).json()

    with col3:
        if st.button('Delete'):
            requests.delete('http://localhost:5000/cards', json=card_json).json()