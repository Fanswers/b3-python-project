import time
from selenium import webdriver
import pandas as pd


# Scrapp one ldlc page (scrapp page 1 : set parameter page to '' / scapp page 2 and superior : set parameter page to 'page2' or 'page3'...
def ldlc_single_page_scrapping(page):
    browser = webdriver.Chrome(r'C:\chromedriver.exe')

    ldlc_url = 'https://www.ldlc.com/informatique/pieces-informatique/carte-graphique-interne/c4684/' + page

    # Time sleep to 0.5 to let page load
    browser.get(ldlc_url)
    time.sleep(0.5)

    # On créé 3 listes des éléments
    articles_name = browser.find_elements_by_class_name('title-3')
    articles_desc = browser.find_elements_by_class_name('desc')
    articles_price = browser.find_elements_by_class_name('basket')
    articles_dispo = browser.find_elements_by_class_name('modal-stock-web')

    names_list = []
    descs_list = []
    prices_list = []
    marketplace_list = []
    dispo_list_all = []

    # On créé 3 listes contenant le texte des éléments
    for title in articles_name:
        names_list.append(title.text)
        marketplace_list.append("ldlc")

    for desc in articles_desc:
        descs_list.append(desc.text)

    for price in articles_price:
        prices_list.append(price.text)

    for dispo in articles_dispo:
        dispo_list_all.append(dispo.text)

    dispo_list = [dispo_list_all[elem] for elem in range(len(dispo_list_all)) if elem % 2 != 0]

    prices_list.pop(0)

    # On créé un DataFrame à partir des 3 listes
    df_graphical_card = pd.DataFrame(
        {
            "name": names_list,
            "description": descs_list,
            "price": prices_list,
            "market_place": marketplace_list,
            "dispo": dispo_list,
        },
    )

    browser.close()

    # Price to float
    df_graphical_card['price'] = df_graphical_card['price'].replace({"€": "."}, regex=True)
    df_graphical_card['price'] = df_graphical_card['price'].replace({" ": ""}, regex=True)
    df_graphical_card['price'] = df_graphical_card['price'].replace({r'\n': ";"}, regex=True)
    df_graphical_card['price'] = df_graphical_card['price'].replace({r'^(.*?);': ""}, regex=True)
    df_graphical_card.index = df_graphical_card.index.astype(int)

    return df_graphical_card


# Scap multiple ldlc with number of page as parameter
def ldlc_multiple_page_scrapping(nb_page):
    df_graphical_card = pd.DataFrame()
    for i in range(nb_page):
        if i == 0:
            page = ""
        else:
            page = "page" + str(i + 1)
        df_graphical_card = pd.concat([df_graphical_card, ldlc_single_page_scrapping(page)], axis=0)

    return df_graphical_card