from flask import Flask
from flask_restful import Resource, Api, reqparse
from src.infrastructure.mariadb.database import database_get, database_insert, full_table_to_dict

# Api definition
app = Flask(__name__)
api = Api(app)

# Allow us to get json argument from method
post_args = reqparse.RequestParser()
post_args.add_argument("card_id")
post_args.add_argument("name")
post_args.add_argument("description")
post_args.add_argument("price")
post_args.add_argument("marketplace")
post_args.add_argument("dispo")


# All cards
class Cards(Resource):
    def get(self):
        return full_table_to_dict("select * from graphic_cards;")

    def post(self):
        args = post_args.parse_args()
        query = "insert into graphic_cards(name, description, price, marketplace, dispo) values('{}', '{}', {}, '{}', '{}')".format(args['name'], args['description'], args['price'], args['marketplace'], args['dispo'])
        database_insert(query)
        return

    def put(self):
        args = post_args.parse_args()
        query = "UPDATE graphic_cards SET name = '{}', description = '{}', price = {}, marketplace = '{}', dispo = '{}' WHERE card_id = {};".format(args['name'], args['description'], args['price'], args['marketplace'], args['dispo'], args['card_id'])
        database_insert(query)
        return

    def delete(self):
        args = post_args.parse_args()
        query = "DELETE FROM graphic_cards WHERE card_id = {};".format(args['card_id'])
        database_insert(query)
        return

# Single card by id
class Card(Resource):
    def get(self, id):
        return full_table_to_dict("select * from graphic_cards;")["card" + str(id)]

# Cards by price
class CardPrice(Resource):
    def get(self, pricemin, pricemax):
        return full_table_to_dict(f"select * from graphic_cards where price > {pricemin} and price < {pricemax};")

# Cards by price and name
class CardPriceName(Resource):
    def get(self, pricemin, pricemax, name):
        return full_table_to_dict(f"select * from graphic_cards where price > {pricemin} and price < {pricemax} and name like '{name}';")

# Cards by price, name and marketplace
class CardPriceNameMarketplace(Resource):
    def get(self, pricemin, pricemax, name, marketplace):
        return full_table_to_dict(f"select * from graphic_cards where price > {pricemin} and price < {pricemax} and name like '{name}' and marketplace like '{marketplace}';")

# Cards by all params
class CardAllParams(Resource):
    def get(self, pricemin, pricemax, name, marketplace, dispo):
        return full_table_to_dict(f"select * from graphic_cards where price > {pricemin} and price < {pricemax} and name like '{name}' and marketplace like '{marketplace}' and dispo like '{dispo}';")

# Card's max price
class MaxPrice(Resource):
    def get(self):
        return {'max_price': database_get("select max(price) from graphic_cards;")[0][0]}


api.add_resource(Cards, '/cards')
api.add_resource(Card, '/cards/<id>')
api.add_resource(CardPrice, '/cards/<pricemin>-<pricemax>')
api.add_resource(CardPriceName, '/cards/<pricemin>-<pricemax>/<name>')
api.add_resource(CardPriceNameMarketplace, '/cards/<pricemin>-<pricemax>/<name>/<marketplace>')
api.add_resource(CardAllParams, '/cards/<pricemin>-<pricemax>/<name>/<marketplace>/<dispo>')
api.add_resource(MaxPrice, '/max_price')

